package test.instawash.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import instawash.app.MainApp;

public class TestMainApp {

	final String nullString = null;
	MainApp main = new MainApp();

	@DisplayName("Test Find Smallest Window String")
	@ParameterizedTest
	@ValueSource(strings = { "racecar", "      ", "", "able was I ere I saw elba", "0", "aabcbcdbca",
			"aa bb cc dd ee" })
	void testfindSmallestWindowString(String s) throws Exception {

		Assertions.assertNotEquals(0, s);
		Assertions.assertNotNull(s);
		Assertions.assertNotEquals(0, main.findSmallestWindowString(s).length());
		Assertions.assertNotEquals(0.0, s, "The input is not 0.0");
		Assertions.assertTrue(true, main.findSmallestWindowString(s));

	}

}
