package instawash.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MainApp {

	public String findSmallestWindowString(String s) throws Exception {

		String result = null;

		if (s == null || s == "" || s.length() <= 1) {
			System.exit(0);
		}

		System.out.println("Original String : " + s);
		List<Character> chars = convertStringToCharList(s);
		Collection<Character> list = chars;
		List<Character> distinctElements = list.stream().distinct().collect(Collectors.toList());
		String distinctString = distinctElements.stream().map(String::valueOf).collect(Collectors.joining());
		result = "The smallest window string is :" + distinctString + "\nThe smallest window size is : "
				+ distinctString.length() + "\n";

		return result;

	}

	public static List<Character> convertStringToCharList(String str) throws Exception {
		List<Character> chars = new ArrayList<>();
		for (char ch : str.toCharArray()) {
			chars.add(ch);
		}
		return chars;
	}

	public static void main(String[] args) throws Exception {
		MainApp main = new MainApp();

		// (for test purpose ) System.out.println(main.findSmallestWindowString("a"));
		System.out.println(main.findSmallestWindowString("ab"));
		System.out.println(main.findSmallestWindowString("aabcbcdbca"));
		System.out.println(main.findSmallestWindowString("aaab"));
		System.out.println(main.findSmallestWindowString("aabbcccceeadallzaao3"));
		System.out.println(main.findSmallestWindowString("    "));
		System.out.println(main.findSmallestWindowString(" aa bb cc dd ee"));
		System.out.println(main.findSmallestWindowString(" a a b b c c d d e e z z"));
		System.out.println(main.findSmallestWindowString(" a a b  c c d d e e z z"));
		System.out.println(main.findSmallestWindowString(""));
		System.out.println(main.findSmallestWindowString(" a a o  c c d d e e z z"));
		System.out.println(main.findSmallestWindowString("0"));

	}

}
